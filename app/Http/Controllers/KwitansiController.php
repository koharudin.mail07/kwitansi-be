<?php

namespace App\Http\Controllers;

use App\Models\Kwitansi;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Str;

class KwitansiController extends Controller
{

    public function edit(ModelsKwitansi $data)
    {
        return response()->json($data, 200);
    }
    public function show(Kwitansi $data)
    {
        return response()->json($data, 200);
    }
    public function destroy(Kwitansi $data)
    {
        $data->delete();
        return response()->json(['message' => 'Data berhasil dihapus'], 200);
    }
    public function update(Kwitansi $data)
    {
        $validator = Validator::make(request()->all(), [
            'from' => 'required',
            'perihal' => 'required',
            'nominal' => 'required',
            'creator_id' => 'required',
            'date_signed' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'message' => 'Gagal Tersimpan',
            ], 422);
        }
        try {
            $data->from = request()->input('from');
            $data->perihal = request()->input('perihal');
            $data->nominal = request()->input('nominal');
            $data->creator_id = request()->input('creator_id');
            $data->date_signed = request()->input('date_signed');

            $data->save();
            return response()->json([
                'status' => 201,
                'message' => 'Data Tersimpan',
                'data' => $data,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Gagal Tersimpan ' . $e,
            ], 500);
        }
    }
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'time',
            'from' => 'required',
            'perihal' => 'required',
            'nominal' => 'required',
            'creator_id' => 'required',
            'date_signed' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'message' => 'Gagal Tersimpan',
            ], 422);
        }

        try {
            $data = new Kwitansi();
            $data->from = request()->input('from');
            $data->perihal = request()->input('perihal');
            $data->nominal = request()->input('nominal');
            $data->creator_id = request()->input('creator_id');
            $data->date_signed = request()->input('date_signed');

            $data->save();
            return response()->json([
                'status' => 201,
                'message' => 'Data Tersimpan',
                'data' => $data,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Gagal Tersimpan ' . $e,
            ], 500);
        }
    }

    public function index()
    {
        $user = Auth::user();
        $q = request()->input('q');
        $query = Kwitansi::query();
        $query->select("id", 'created_at', 'updated_at');
        //$query->where('creator', $user->id);
        $query->orderBy('updated_at', 'DESC');
        //$query->where('title', 'like', "%" . strtolower($q) . "%");
        $result = $query->paginate(10);
        return $result;
    }
}
