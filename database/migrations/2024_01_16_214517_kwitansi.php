<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kwitansi', function (Blueprint $table) {
            $table->uuid("id");
            $table->string('from');
            $table->string('name');
            $table->float("nominal");
            $table->string("perihal", 500);
            $table->integer("creator_id");
            $table->timestamp("date_signed");
            $table->timestamps();
            $table->softDeletes(); // THIS ONE
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
