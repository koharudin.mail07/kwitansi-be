<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ebook </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://jendela.kemdikbud.go.id/themes/default/css/flipbook/font-awesome.css">
    <link rel="stylesheet" href="https://jendela.kemdikbud.go.id/themes/default/css/flipbook/flipbook.style.css">

    <script src="https://jendela.kemdikbud.go.id/themes/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="https://jendela.kemdikbud.go.id/themes/default/js/flipbook/flipbook.min.js"></script>
</head>

<body class="antialiased">
    <center style="margin-top: 5px;">
        <a href="https://jendela.kemdikbud.go.id/v2/indeks/downloadmag/?seo=edisi-65-juni-2023-merdekabelajar-merdeka-belajar-hardiknas2023" target="_blank" class="btn btn-warning">Download <span class="badge">1591</span></a>
        <a href="#" class="btn btn-labeled btn-primary" id="80"><span class="btn-label"><i class="glyphicon glyphicon-eye-open"></i> </span>Baca</a>
    </center>
</body>
<!-- bootstrab js -->
<script src="https://jendela.kemdikbud.go.id/assets/v2/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#80").flipBook({
            pdfUrl: "http://localhost:8000/MAJALAH_JENDELA_EDISI_65-JULI_2023_OKE_interaktif.pdf",
            lightBox: true,
            lightBoxFullscreen: true

        });
    });
</script>

</html>